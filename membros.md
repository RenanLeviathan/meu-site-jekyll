---
layout: pagina
title: Membros
---
{% for membro in site.data.membros%}
<span>[{{membro.nome}}](membros/{{membro.nome | slugify: "latin"}}) - {{membro.funcao}} </span>
{%endfor%}
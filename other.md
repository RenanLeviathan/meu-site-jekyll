---
layout: pagina
title: otherpage
---

{{page.title}} é outra página. Para não refazer o cabeçalho (`<header>`), basta incluir o HTML do elemento de template que deseja. Melhor ainda, crie um layout com o _header_ que quiser ^\_^
---
layout: pagina
title: Tutorial Jekyll
---

`code snippet`\
[Galinha](imgs/19061.jpg) \
[Página avulsa](page.html)\
os posts devem ser **diretamente** inseridos ~~lá~~ na pasta `_posts`\
# Imagens
use a tag `<img>`\
<img src="imgs/19061.jpg"/>
# Tabelas
Recomenda-se usar HTML para tal. Exemplo:
<table>
	<tbody>
		<tr><td>Linha 1 Coluna 1</td><td>Linha 1 Coluna 2</td></tr>
	</tbody>
</table>

A tabela foi renderizada pelo layout {{page.layout}}
2+2={{page.number}}\
# Membros
[Membros do projeto](membros.html)
# Acessando às imagens de `assets/ibagens`

{% assign image_files = site.static_files | where: "image", true %}
{% for myimage in image_files %}
  <img src="{{ myimage.path | relative_url}}" width=200 height=200/>
{% endfor %}

# Reutilizando HTML

[este link]({%link other.md%}) lhe mostrará como

# Realce de linguagens
## Python
{% highlight python%}
def foo:
  print 'foo'
{% endhighlight %}
## Java
{% highlight python%}
String foo(){
  System.out.println("foo");
}
{% endhighlight %}

# Ligação com postagens

[veja este post]({%post_url 2020-01-04-ola%})